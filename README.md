# Accurate iris segmentation for at-a-distance acquired iris/face images under less constrained environment 


**Abstract.** Iris recognition for at-a-distance acquired iris images under less constrained environment has shown to be challenging due to highly imaging variations such as reflections, motion blur, occlusions etc. This poses challenges for conventional gradient-based iris segmentation methods which are essentially developed to work on high quality iris images acquired in a controlled environment. In this work, we propose an effective encoder-decoder Deep Convolutional Neural Network which can be trained end-to-end to perform iris segmentation for distantly acquired iris/face images. More specifically, the proposed approach is motivated by the recent state-of-the-art semantic segmentation approach – DeepLabv3/3+. The encoder module adapts the ResNet-50 as base network and extended with additional blocks constructed using multi-grid atrous convolution, and Atrous Spatial Pyramid Pooling to capture multi-scale features, which can better accommodate the segmentation of iris at different scales. To facilitate recovering of the spatial information, refinement module is introduced in the decoder module. We demonstrate the effectiveness of the proposed approach on two public datasets, i.e., UBIRIS.v2 and FRGC, which achieves average improvement of 37.42% and 48.9%, respectively. The trained model is made publicly available at **[here](https://gitlab.com/cwtan501/iris_segmentation/tree/master/model)** to encourage reproducible of the reported results.

- - -

**Table 1. [Average segmentation error, E](http://nice1.di.ubi.pt/evaluation.htm), of different methods**

|                 | **UBIRIS.v2**      | **FRGC**       |
| --------------- | ---------------   |--------------- |
| Method [1]      | 2.13%             | 1.71%          |
| Method [2]      | 1.72%             | 1.76%          |
| Method [3]      | 1.21%             | 1.27%          |
| Proposed method | **1.00%**         | **0.79%**      |

- - -

 **Sample segmentation results**
 
 ![UBIRIS.v2](images/sample_outputs.jpg)
 
 
 
 - - -
 
 **References**
 1. L.-C. Chen, Y. Zhu, G. Papandreou, F. Schroff, and H. Adam, “Rethinking Atrous Convolution for Semantic Image Segmentation Liang-Chieh,” arXiv.org, 2018.
 2. C. W. Tan and A. Kumar, “Towards online iris and periocular recognition under relaxed imaging constraints,” IEEE Trans. Image Process., vol. 22, no. 10, pp. 3751–3765, 2013.
 3. Z. Zhao and A. Kumar, “An accurate iris segmentation framework under relaxed imaging constraints using total variation model,” in Proceedings of the IEEE International Conference on Computer Vision, 2015.