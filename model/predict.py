import tensorflow as tf
from tensorflow.keras.models import load_model
import os

from tensorflow.keras.preprocessing import image as kimage
from tensorflow.keras.applications.resnet50 import preprocess_input as resnet50_preprocessor
import numpy as np
from PIL import Image



preprocessor_func = resnet50_preprocessor


# define flags using absl-py
from absl import app, flags, logging
FLAGS = flags.FLAGS
flags.DEFINE_string('model', None, 'model path')
flags.DEFINE_string('image_dir', None, 'image directory')
flags.DEFINE_string('out_dir', None, 'output directory')
flags.DEFINE_list('target_sz', '224,224,3', 'target image size')


flags.mark_flags_as_required(['model', 'image_dir', 'out_dir'])


def calc_E1_error(y_true, y_pred):
	threshold = 0.5
	O = tf.greater(y_true, threshold)
	C = tf.math.argmax(y_pred, axis = 3)
	C = tf.expand_dims(C, axis = 3)
	C = tf.cast(C, tf.bool)
	
	shape = tf.shape(y_true)
	cr = tf.cast(shape[1] * shape[2], tf.float32)	
	xor = tf.math.logical_xor(O, C)	

	e1_error = tf.cast(tf.math.count_nonzero(xor, axis = (1,2,3)), tf.float32) / cr
	return tf.reduce_mean(e1_error) # mean E1 error
# end def


def process_pred(pred, target_sz = None, interpolation = Image.LANCZOS, threshold = 128):
    val = np.argmax(pred, axis = -1) * 255
    val = np.expand_dims(val, axis = 2)
    if target_sz:        
        val = kimage.array_to_img(val)
        val = val.resize(target_sz, interpolation)
        val = np.array(val) >= threshold
        val = np.expand_dims(val, axis = 2)
    # end
    return val.astype(np.uint8)
# end def

def str2int(a_list):
    return [int(v) for v in a_list]
# end

def main(argv):

    model_path  = FLAGS.model
    image_dir   = FLAGS.image_dir
    out_dir     = FLAGS.out_dir
    target_sz   = str2int(FLAGS.target_sz)[:3]

    # load model
    logging.info('Loading model')
    model = load_model(model_path, custom_objects = {'calc_E1_error' : calc_E1_error})
    logging.info('Model successfully loaded')
 
    if not os.path.exists(out_dir):
        util.mkdirs(out_dir)
    
    img_list  = os.listdir(image_dir)
	
	
    logging.info('Total test images: {}'.format(len(img_list)))
    logging.info('Start segmentation')

    for idx in range(len(img_list)):
        filename = os.path.basename(img_list[idx])
        
        img = kimage.load_img(img_list[idx])
        img_sz_ori = img.size # capture ori image size
		
        img = img.resize(target_sz[:2], Image.NEAREST)        
        img = kimage.img_to_array(img).astype(np.float32)
        
        img = np.expand_dims(img, axis = 0)
        img = preprocessor_func(img)
        pred = model.predict(img)
        pred = process_pred(pred[0], target_sz = img_sz_ori)

        out_fn = os.path.join(out_dir, '{}'.format(filename))
        pred = kimage.array_to_img(pred)
        pred.save(out_fn)

    # end for

    logging.info('Segmentation completed')

# end


if __name__ == '__main__':

    app.run(main)
        

    # end