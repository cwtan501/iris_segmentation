### Model
The model size is too big to be fitted here and therefore I provide a separate link to download (will make available soon). it instead. The model is implemented based on Keras with Tensorflow 2.0 as backend. 
The model expects input image size of 224×224 and prediction output will be in the shape of 224×224×2.

A sample script (predict.py) is provided to run the prediction:

    python3 predict.py --model [*path_to_model*] --image_dir [*path_to_image_dir*] --out_dir [*path_to_save_prediction_output*]


**Disclaimer**
The model provided is made publicly available to be used solely for research purposes to encourage reproducible of the reported results.
The model is provided on "as it is" basis and does not include any warranty of any kind.